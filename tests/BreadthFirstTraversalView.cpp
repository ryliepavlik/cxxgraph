// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Tests for cxxgraph/TraversalView.h using BreadthFirstTraversal
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

// Internal Includes
#include "Configs.h"
#include "Matcher.h"

#include "cxxgraph/AdjacencyListGraph.h"
#include "cxxgraph/BreadthFirstTraversal.h"
#include "cxxgraph/Stream.h"
#include "cxxgraph/TraversalView.h"

// Library Includes
#include <catch2/catch.hpp>
#include <range/v3/algorithm.hpp>

// Standard Includes
#include <sstream>

using namespace cxxgraph::traversal;
using traversal_t = BreadthFirstTraversal<cxxgraph::adjacency_list::Graph<my_config::VecVecStringStringConfig>>;
using cursor_t = decltype(std::declval<TraversalView<traversal_t>&>().begin_cursor());
CONCEPT_ASSERT(ranges::detail::Cursor<cursor_t>());
CONCEPT_ASSERT(ranges::detail::ReadableCursor<cursor_t>());
CONCEPT_ASSERT(ranges::detail::InputCursor<cursor_t>());

namespace {
/*

Graph:

a  ->  b

|      |
v      v

c  ->  d

*/
template <typename GraphConfig>
struct BFTViewFixture
{

	using Graph = cxxgraph::adjacency_list::Graph<GraphConfig>;
	using BFT = BreadthFirstTraversal<Graph>;
	using vertex_const_descriptor_t = cxxgraph::graph_vertex_const_descriptor_t<Graph>;
	using edge_const_descriptor_t = cxxgraph::graph_edge_const_descriptor_t<Graph>;

	BFTViewFixture()
	    : g(), a(g.addVertex("a")), b(g.addVertex("b")), c(g.addVertex("c")), d(g.addVertex("d")),
	      ab(g.addEdge(a, b, "ab")), ac(g.addEdge(a, c, "ac")), cd(g.addEdge(c, d, "cd")), bd(g.addEdge(b, d, "bd"))
	{}

	Graph g;
	vertex_const_descriptor_t a;
	vertex_const_descriptor_t b;
	vertex_const_descriptor_t c;
	vertex_const_descriptor_t d;

	edge_const_descriptor_t ab;
	edge_const_descriptor_t ac;
	edge_const_descriptor_t cd;
	edge_const_descriptor_t bd;

	template <typename Rng>
	void verifyViewFromA(Rng&& rng) const
	{
		std::size_t i = 0;
		ranges::for_each(rng, [&](auto v) {
			INFO("Loop iteration " << i);
			switch (i) {
			case 0:
				// step 0 is always a
				REQUIRE(v == a);
				break;
			case 1:
			case 2:
				// step 1 and 2 are b and c, in some undefined order.
				REQUIRE_THAT(v, IsInSet({b, c}));
				break;
			case 3:
				// step 3 is always d
				REQUIRE(v == d);
				break;
			default:
				FAIL("Got step number " << i);
			}
			++i;
		});

		i = 0;
		for (auto v : rng) {
			INFO("Loop iteration " << i);
			switch (i) {
			case 0:
				// step 0 is always a
				REQUIRE(v == a);
				break;
			case 1:
			case 2:
				// step 1 and 2 are b and c, in some undefined order.
				REQUIRE_THAT(v, IsInSet({b, c}));
				break;
			case 3:
				// step 3 is always d
				REQUIRE(v == d);
				break;
			default:
				FAIL("Got step number " << i);
			}
			++i;
		}
	}

	template <typename Rng>
	void verifyViewFromBUndirected(Rng&& rng) const
	{
		std::size_t i = 0;
		ranges::for_each(rng, [&](auto v) {
			INFO("Loop iteration " << i);
			switch (i) {
			case 0:
				// step 0 is always b
				REQUIRE(v == b);
				break;
			case 1:
			case 2:
				// step 1 and 2 are a and d, in some undefined order.
				REQUIRE_THAT(v, IsInSet({a, d}));
				break;
			case 3:
				// step 3 is always c
				REQUIRE(v == c);
				break;
			default:
				FAIL("Got step number " << i);
			}
			++i;
		});

		i = 0;
		for (auto v : rng) {
			INFO("Loop iteration " << i);
			switch (i) {
			case 0:
				// step 0 is always b
				REQUIRE(v == b);
				break;
			case 1:
			case 2:
				// step 1 and 2 are a and d, in some undefined order.
				REQUIRE_THAT(v, IsInSet({a, d}));
				break;
			case 3:
				// step 3 is always c
				REQUIRE(v == c);
				break;
			default:
				FAIL("Got step number " << i);
			}
			++i;
		}
	}
	template <typename Rng>
	void verifyViewFromDReverseDirected(Rng&& rng) const
	{
		std::size_t i = 0;
		ranges::for_each(rng, [&](auto v) {
			INFO("Loop iteration " << i);
			switch (i) {
			case 0:
				// step 0 is always d
				REQUIRE(v == d);
				break;
			case 1:
			case 2:
				// step 1 and 2 are b and c, in some undefined order.
				REQUIRE_THAT(v, IsInSet({b, c}));
				break;
			case 3:
				// step 3 is always a
				REQUIRE(v == a);
				break;
			default:
				FAIL("Got step number " << i);
			}
			++i;
		});

		i = 0;
		for (auto v : rng) {
			INFO("Loop iteration " << i);
			switch (i) {
			case 0:
				// step 0 is always d
				REQUIRE(v == d);
				break;
			case 1:
			case 2:
				// step 1 and 2 are b and c, in some undefined order.
				REQUIRE_THAT(v, IsInSet({b, c}));
				break;
			case 3:
				// step 3 is always a
				REQUIRE(v == a);
				break;
			default:
				FAIL("Got step number " << i);
			}
			++i;
		}
	}

	void testDirected() const
	{
		WHEN("Traversing (directed) from a")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromA(traversalView<BreadthFirstTraversal>(g, a));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromA(traversalView(g, BreadthFirstTraversal<Graph>{g}, a));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(a);
				verifyViewFromA(traversalView(g, std::move(strategy)));
			}
		}
	}

	void testBidi() const
	{
		WHEN("Traversing as a directed graph from a")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromA(
				    traversalView<BreadthFirstTraversal>(g, a, cxxgraph::DirectedBehavior::Directed));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromA(traversalView(g, BreadthFirstTraversal<Graph>{g}, a,
				                              cxxgraph::DirectedBehavior::Directed));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(a);
				verifyViewFromA(
				    traversalView(g, std::move(strategy), cxxgraph::DirectedBehavior::Directed));
			}
		}
		WHEN("Traversing bidirectionally from a")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromA(traversalView<BreadthFirstTraversal>(
				    g, a, cxxgraph::DirectedBehavior::Bidirectional));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromA(traversalView(g, BreadthFirstTraversal<Graph>{g}, a,
				                              cxxgraph::DirectedBehavior::Bidirectional));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(a);
				verifyViewFromA(
				    traversalView(g, std::move(strategy), cxxgraph::DirectedBehavior::Bidirectional));
			}
		}
		WHEN("Traversing bidirectionally from b")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromBUndirected(traversalView<BreadthFirstTraversal>(
				    g, b, cxxgraph::DirectedBehavior::Bidirectional));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromBUndirected(traversalView(g, BreadthFirstTraversal<Graph>{g}, b,
				                                        cxxgraph::DirectedBehavior::Bidirectional));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(b);
				verifyViewFromBUndirected(
				    traversalView(g, std::move(strategy), cxxgraph::DirectedBehavior::Bidirectional));
			}
		}
		WHEN("Traversing reverse-directed from d")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromDReverseDirected(traversalView<BreadthFirstTraversal>(
				    g, d, cxxgraph::DirectedBehavior::ReverseDirected));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromDReverseDirected(
				    traversalView(g, BreadthFirstTraversal<Graph>{g}, d,
				                  cxxgraph::DirectedBehavior::ReverseDirected));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(d);
				verifyViewFromDReverseDirected(
				    traversalView(g, std::move(strategy), cxxgraph::DirectedBehavior::ReverseDirected));
			}
		}
	}
	void testUndir() const
	{
		WHEN("Traversing from a")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromA(traversalView<BreadthFirstTraversal>(g, a));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromA(traversalView(g, BreadthFirstTraversal<Graph>{g}, a));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(a);
				verifyViewFromA(traversalView(g, std::move(strategy)));
			}
		}
		WHEN("Traversing from b")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromBUndirected(traversalView<BreadthFirstTraversal>(g, b));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromBUndirected(traversalView(g, BreadthFirstTraversal<Graph>{g}, b));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(b);
				verifyViewFromBUndirected(traversalView(g, std::move(strategy)));
			}
		}
		WHEN("Traversing from d")
		{
			AND_WHEN("Calling the traversalView() overload that does not take a Strategy instance")
			{
				verifyViewFromDReverseDirected(traversalView<BreadthFirstTraversal>(g, d));
			}

			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance and a source")
			{
				verifyViewFromDReverseDirected(traversalView(g, BreadthFirstTraversal<Graph>{g}, d));
			}
			AND_WHEN("Calling the traversalView() overload that takes a Strategy instance")
			{
				BreadthFirstTraversal<Graph> strategy{g};
				strategy.enqueueRoot(d);
				verifyViewFromDReverseDirected(traversalView(g, std::move(strategy)));
			}
		}
	}
};
} // namespace

TEST_CASE_METHOD(BFTViewFixture<my_config::VecVecStringStringConfig>, "BFT-View-VecVecStringString") { testDirected(); }

TEST_CASE_METHOD(BFTViewFixture<my_config::BidiVecVecStringStringConfig>, "BFT-View-BidiVecVecStringString")
{
	testBidi();
}

TEST_CASE_METHOD(BFTViewFixture<my_config::UndirVecVecStringStringConfig>, "BFT-View-UndirVecVecStringString")
{
	testUndir();
}

TEST_CASE_METHOD(BFTViewFixture<my_config::ListVecStringStringConfig>, "BFT-View-ListVecStringString")
{
	testDirected();
}

TEST_CASE_METHOD(BFTViewFixture<my_config::VecListStringStringConfig>, "BFT-View-VecListStringString")
{
	testDirected();
}

TEST_CASE_METHOD(BFTViewFixture<my_config::BidiVecListStringStringConfig>, "BFT-View-BidiVecListStringString")
{
	testBidi();
}

TEST_CASE_METHOD(BFTViewFixture<my_config::UndirVecListStringStringConfig>, "BFT-View-UndirVecListStringString")
{
	testUndir();
}

TEST_CASE_METHOD(BFTViewFixture<my_config::SmListVecStringStringConfig>, "BFT-View-SmListVecStringString")
{
	testDirected();
}
