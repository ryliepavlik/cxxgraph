
# Main Page {#mainpage}

## Introduction

This is a modern C++ generic graph (vertex-and-edge, not charting) library.
Some design decisions were inspired by [Boost.Graph][],
but the overall library is simpler and in a more modern style.
(It's also less comprehensive, but should be easy to build on.)

If you're viewing this on the web (at <https://ryanpavlik.gitlab.io/cxxgraph/>),
you can view the code coverage data for the most recent master-branch build
[here](https://ryanpavlik.gitlab.io/cxxgraph/coverage/)

## Graph Storage and Descriptors

Like in [Boost.Graph][], a particular graph type stores some number of edges and vertices,
as well as optionally internally-stored properties (in a user-defined structure) for each edge or vertex.
Edges and vertices are referred to with "descriptors",
specially-typed values that, together with the graph class itself,
allow access to structure and properties associated with the corresponding edge/vertex.
They can also be used as keys in a "descriptor map" -
for instance, when computing or storing data per-vertex external to the graph's internal properties.
You can think of descriptors as being essentially index values into some graph storage.
(Behind the scenes, depending on how you've configured the graph to store the corresponding entities,
they might be a type-safe wrapper around an index or a pointer.)
However, you typically will not have to interact with descriptors beyond using them as keys or comparing them:
accessing their corresponding entity ("dereferencing" them) is always done through a graph method call.
All descriptor types can be constructed with no parameters (default-constructible),
and a default-constructed descriptor is be a unique, universal "invalid" sentinel value,
that will never be associated with a real entity.
The specific type of a descriptor is determined by
the configured way the corresponding entities (vertices or edges) are stored.
For example, vector-backed storage uses a type-safe wrapper around an integer index
(with a `std::numeric_limits::max<index_type>()` sentinel),
while list-backed storage uses a type-safe wrapper
(distinguishing edge and vertex descriptors, as well as stored data/property types)
around a pointer (with `nullptr` as sentinel).

You can refer to the type of vertex and edge descriptors
(in non-const and const variations respectively)
for any graph type using the following template aliases:

```{.cpp}
cxxgraph::graph_vertex_descriptor_t<YourGraphType>
cxxgraph::graph_vertex_const_descriptor_t<YourGraphType>
cxxgraph::graph_edge_descriptor_t<YourGraphType>
cxxgraph::graph_edge_const_descriptor_t<YourGraphType>
```

(Typically you don't have to worry about the distinction between const and non-const descriptors,
since it is the constness of the overall graph structure that matters.)

## Ranges

Ranges, using the popular [range-v3][] library, are an important part of the library's interface,
as well as a major difference from [Boost.Graph][].
Ranges are used most trivially
as a replacement for pairs of `begin()` and `end()` iterators that may be used with range-for,
but they also provide transformations, including in a concise "pipeline" style.
A full exploration of ranges is not appropriate here, but documentation and samples for range-v3 are available.
Many methods that return ranges return a range of pairs,
where the first element of each pair (typically) is a descriptor while the second is a reference to the corresponding object.
If iterating over such a range, you will typically iterate by value,
and use free functions in the `cxxgraph` namespace to access the elements of the pair,
instead of manually accessing `.first` and `.second`, for ease of maintenance and readability.
See @ref freeaccessors for a list of the available free-function accessors.

## Static Polymorphism

Another difference between cxxgraph and Boost.Graph is
this library's widespread use of the ["Curiously Recurring Template Pattern"][CRTP], or CRTP for short.
It is used as a way to write generic code that provides shared implementations and optimized implementations,
as well as uniform interfaces across diverse implementations without runtime dispatching (virtual functions).
While you can mostly ignore this if you are just using existing code from the library,
rather than contributing or writing generic code,
you may see references to it.
The gist is that some interfaces are provided as a class/struct template,
with one of the template parameters being the implementation/derived class.
Due to the order of instantiation, the interface/base class can
call methods of the derived class without any runtime performance cost,
because the derived type is known in the template class it is deriving from.
For example, graph types inherit from the `GraphBase<Derived>` class template,
so a graph type might be defined as

```{.cpp}
template<typename GraphConfig>
class MyGraphType : public GraphBase<MyGraphType<GraphConfig>> {
  // ...
};
```

Where you're most likely to interact with the CRTP, if you aren't contributing code to the library,
is in function prototypes. For example, a function that takes a graph might be defined as:

```{.cpp}
template<typename Derived>
inline void myGraphFunction(GraphBase<Derived> const& g) {
  // interact with g.derived() which returns a reference to const Derived
}
```

You can treat this as a function that generically takes in any graph type,
since all `Derived` graph types inherit from `GraphBase<Derived>`
and can be passed to a function expecting a reference to `GraphBase<Derived>`.
The word `Derived` in this declaration is not important,
it's just a type parameter name.
While functions interacting generically with types via the CRTP must be function templates,
there is no need to explicitly specify the type parameter when calling them,
since they are deduced automatically by the compiler.
The above function may thus be called with

```{.cpp}
MyGraphType<myconfig> myGraph;
myGraphFunction(myGraph);
```

and the compiler will deduce the type for `Derived` and generate a specialization automatically.

`derived()` is a method provided by all CRTP bases in `cxxgraph`,
which provides access to a (`const`-qualified, if appropriate) reference to the object
as its implementation (derived) type.

```{.cpp}
MyGraphType<myconfig> myGraph;
static_assert(std::is_same<decltype(g.derived()), MyGraphType&>);

template<typename Derived>
inline void myGraphFunction(GraphBase<Derived> const& g) {
  // When myGraph is passed as g:
  static_assert(std::is_same<decltype(g.derived()), MyGraphType const&>);
}
```

In functions taking a CRTP base, you will typically want to not use passed parameters directly,
but rather interact with the return of the `derived()` method.
For example:

```{.cpp}
template<typename Derived>
inline void myGraphFunction(GraphBase<Derived> const& g) {
  for (auto p : g.derived().vertices()) {
    auto vertexDescriptor = getVertexDescriptor(p);
    auto & props = getProperties(p);
  }
  // When myGraph is passed as g:
  static_assert(std::is_same<decltype(g.derived()), MyGraphType const&>);
}
```

[range-v3]: https://github.com/ericniebler/range-v3
[Boost.Graph]: https://www.boost.org/doc/libs/1_68_0/libs/graph/doc/index.html
[CRTP]: https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern
