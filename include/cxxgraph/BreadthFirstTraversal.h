// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Traversal.h"

// Library Includes
// - none

// Standard Includes
#include <deque>

namespace cxxgraph::traversal {

/*! Traversal strategy to perform a breadth-first search or traversal.
 */
template <typename Graph>
struct BreadthFirstTraversal : TraversalStrategyBase<BreadthFirstTraversal<Graph>, Graph>
{
	using base_t = TraversalStrategyBase<BreadthFirstTraversal<Graph>, Graph>;
	using graph_t = Graph;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<Graph>;
	using predecessor_map_t = graph_descriptor_map_t<Graph, vertex_tag, vertex_const_descriptor_t>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<Graph>;

	//! Constructor
	explicit BreadthFirstTraversal(Graph const& graph) : predecessors(graph) {}

	template <typename F>
	auto dequeueAndVisit(F && /*earlyStoppingPredicate*/) -> state_vertex_descriptor_pair_t<graph_t>
	{
		if (queue.empty()) {
			return traversal_finished_v<graph_t>;
		}
		auto front = queue.front();
		queue.pop_front();
		return {ExecutionState::Proceed, front};
	}

	template <typename F>
	auto enqueue(edge_const_descriptor_t /*e*/, vertex_const_descriptor_t prev, vertex_const_descriptor_t v,
	             F&& earlyStoppingPredicate) -> ExecutionState
	{
		if (!isQueued(v)) {
			// Only queue if we haven't already queued this vertex.
			predecessors[v] = prev;
			queue.push_back(v);
			if (std::forward<F>(earlyStoppingPredicate)(v)) {
				return ExecutionState::EarlyStop;
			}
		}
		return ExecutionState::Proceed;
	}

	/*! True if we have queued v.
	 *
	 * Note that all visited vertices have been queued.
	 */
	bool isQueued(vertex_const_descriptor_t v) const { return predecessors[v] != vertex_const_descriptor_t{}; }

	predecessor_map_t predecessors;
	std::deque<vertex_const_descriptor_t> queue;
};
} // namespace cxxgraph::traversal
