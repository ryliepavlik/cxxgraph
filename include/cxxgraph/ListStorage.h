// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"

// Library Includes
#include <range/v3/core.hpp>
#include <range/v3/view/transform.hpp>

// Standard Includes
#include <algorithm>
#include <list>
#include <unordered_map>
#include <vector>

namespace cxxgraph::policy {
struct PointerDescriptorPrintable
{
	void const* ptr;
};

//! Type-safe wrapper indicating a pointer is an edge or vertex descriptor.
template <typename Tag, typename Pointer>
struct PointerDescriptor : cxxgraph::detail::DescriptorBase<PointerDescriptor<Tag, Pointer>, Tag>
{

	using tag_type = Tag;
	using pointer_t = Pointer;
	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be edge_tag or vertex_tag");

	static_assert(std::is_pointer<Pointer>());
	static constexpr bool is_const_v = std::is_const<std::remove_pointer_t<Pointer>>();
	static constexpr detail::Constness constness =
	    is_const_v ? detail::Constness::Const : detail::Constness::NonConst;

	//! Called by shared stream output functions.
	constexpr PointerDescriptorPrintable printable() const noexcept { return {ptr}; }

	//! Pointer to non-const.
	using pointer_to_nonconst_t = std::add_pointer_t<std::remove_const_t<std::remove_pointer_t<pointer_t>>>;

	//! Default constructor
	constexpr PointerDescriptor() noexcept = default;
	//! Constructor from wrapped pointer type.
	constexpr PointerDescriptor(Pointer p) noexcept : ptr(p) {}
	/*! Implicit conversion constructor from non-const version of wrapped pointer type.
	 *
	 * Permits implicitly "adding const"
	 */
	constexpr PointerDescriptor(PointerDescriptor<Tag, pointer_to_nonconst_t> const& p) noexcept : ptr(p.ptr) {}
	Pointer ptr = nullptr;

	/*! Assignment operator that implicitly adds const
	 */
	template <typename Ptr, GRAPH_REQUIRES_(std::is_same<traits::stripped_t<Ptr>,
	                                                     traits::stripped_t<pointer_t>>::value&& is_const_v)>
	constexpr PointerDescriptor& operator=(PointerDescriptor<Tag, Ptr> const& p) noexcept
	{
		ptr = p.ptr;
		return *this;
	}

	/*! Equality comparison for PointerDescriptor<>.
	 *
	 * Delegates to underlying descriptor type's equality
	 * comparison.
	 */
	constexpr bool operator==(PointerDescriptor const& other) const noexcept { return ptr == other.ptr; }

	/*! Inequality comparison for PointerDescriptor<>.
	 *
	 * Delegates to underlying descriptor type's inequality
	 * comparison.
	 */
	constexpr bool operator!=(PointerDescriptor const& other) const noexcept { return ptr != other.ptr; }

	/*! Less-than comparison for PointerDescriptor<>.
	 *
	 * Delegates to underlying descriptor type's less-than
	 * comparison.
	 */
	constexpr bool operator<(PointerDescriptor const& other) const noexcept { return ptr < other.ptr; }
};

/*! Overload to explicitly cast away constness from a pointer wrapped in a PointerDescriptor<>
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename PointedToType>
inline PointerDescriptor<Tag, std::add_pointer_t<PointedToType>>
remove_constness(PointerDescriptor<Tag, PointedToType const*> d)
{
	return {const_cast<std::add_pointer_t<PointedToType>>(d.ptr)};
}

/*! Null pointer equality comparison function for PointerDescriptor<>.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
constexpr bool operator==(std::nullptr_t, PointerDescriptor<Tag, Pointer> p)
{
	return p.ptr == nullptr;
}

//! @overload
template <typename Tag, typename Pointer>
constexpr bool operator==(PointerDescriptor<Tag, Pointer> p, std::nullptr_t)
{
	return p.ptr == nullptr;
}

/*! Null pointer inequality comparison function for PointerDescriptor<>.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
constexpr bool operator!=(std::nullptr_t, PointerDescriptor<Tag, Pointer> p)
{
	return p.ptr != nullptr;
}

/*! Null pointer inequality comparison function for PointerDescriptor<>.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
constexpr bool operator!=(PointerDescriptor<Tag, Pointer> p, std::nullptr_t)
{
	return p.ptr != nullptr;
}

/*! Swap function for PointerDescriptor<>, found via ADL.
 *
 * @relates PointerDescriptor
 */
template <typename Tag, typename Pointer>
inline void swap(PointerDescriptor<Tag, Pointer>& a, PointerDescriptor<Tag, Pointer>& b)
{
	using std::swap;
	swap(a.ptr, b.ptr);
}
} // namespace cxxgraph::policy

namespace std {
template <typename Tag, typename Pointer>
struct hash<cxxgraph::policy::PointerDescriptor<Tag, Pointer>>
{
	using argument_type = cxxgraph::policy::PointerDescriptor<Tag, Pointer>;
	constexpr auto operator()(argument_type const& d) const noexcept { return std::hash<Pointer>{}(d.ptr); }
};
} // namespace std

namespace cxxgraph::policy {

/*! Element storage policy that stores elements in a std::list
 * and uses a wrapped pointer-to-element as the descriptor.
 *
 * Uses std::unordered_map for descriptor maps.
 *
 * @see SmallListElementStoragePolicy
 *
 * @ingroup storage_policies
 */
struct ListElementStoragePolicy;

/*! Element storage policy that stores elements in a std::list
 * and uses a wrapped pointer-to-element as the descriptor.
 *
 * Uses an unordered std::vector for descriptor maps, so suitable for "small" collections.
 *
 * @see ListElementStoragePolicy
 *
 * @ingroup storage_policies
 */
struct SmallListElementStoragePolicy;

//! Element storage traits associated with ListElementStoragePolicy
template <typename Tag, typename ValueType>
struct ElementStorageTraits<ListElementStoragePolicy, Tag, ValueType>
{
	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be either edge_tag or vertex_tag.");
	using container_t = std::list<ValueType>;
	using descriptor_t = PointerDescriptor<Tag, std::add_pointer_t<ValueType>>;
	using const_descriptor_t = PointerDescriptor<Tag, std::add_pointer_t<std::add_const_t<ValueType>>>;

	static constexpr bool absolute_descriptors = true;

	template <typename... Args>
	static descriptor_t emplace(container_t& elements, Args... a)
	{
		elements.emplace_back(std::forward<Args>(a)...);
		auto it = elements.end();
		--it;
		return {&(*it)};
	}

	static ValueType& at(descriptor_t d)
	{
		if (nullptr == d) {
			throw std::invalid_argument("Cannot access null descriptor.");
		}
		return *(d.ptr);
	}

	static ValueType& at(container_t& /*elements*/, descriptor_t d) { return at(d); }

	static ValueType const& at(const_descriptor_t d)
	{
		if (nullptr == d) {
			throw std::invalid_argument("Cannot access null descriptor.");
		}
		return *(d.ptr);
	}

	static ValueType const& at(container_t const& /*elements*/, const_descriptor_t d) { return at(d); }

	static auto getRange(container_t const& elements)
	{
		auto wrapPair = [](auto&& e) { return std::make_pair(const_descriptor_t{&e}, std::ref(e)); };
		return ranges::view::transform(elements, wrapPair);
	}

	static auto getRange(container_t& elements)
	{
		auto wrapPair = [](auto&& e) { return std::make_pair(descriptor_t{&e}, std::ref(e)); };
		return elements | ranges::view::transform(wrapPair);
	}

	static auto getDescriptors(container_t const& elements)
	{
		auto makeDescriptor = [](auto&& e) { return const_descriptor_t{&e}; };
		return elements | ranges::view::transform(makeDescriptor);
	}
};

//! Element storage for small list is same as for list
template <typename Tag, typename ValueType>
struct ElementStorageTraits<SmallListElementStoragePolicy, Tag, ValueType>
    : ElementStorageTraits<ListElementStoragePolicy, Tag, ValueType>
{
};

/*! Descriptor map associated with ListElementStoragePolicy.
 *
 * Descriptor maps are always std::unordered_map in this case, at least for now.
 */
template <typename Tag, typename ValueType, typename MapValueType>
class DescriptorMap<ListElementStoragePolicy, Tag, ValueType, MapValueType>
{
	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be either edge_tag or vertex_tag.");
	using storage_traits_t = ElementStorageTraits<ListElementStoragePolicy, Tag, ValueType>;
	using const_descriptor_t = typename storage_traits_t::const_descriptor_t;
	using descriptor_map_t = std::unordered_map<const_descriptor_t, MapValueType>;
	descriptor_map_t map_;

	const MapValueType defaultValue_{};

public:
	DescriptorMap() = default;

	template <typename T>
	DescriptorMap(T const& descriptors, MapValueType const& value, DescriptorRangeTag const& /* dummy */)
	    : defaultValue_(value)
	{
		for (auto d : descriptors) {
			map_[d] = value;
		}
	}

	template <typename T>
	DescriptorMap(T const& descriptors, DescriptorRangeTag const& /* dummy */)
	    : DescriptorMap(descriptors, MapValueType{}, descriptor_range)
	{}

	template <typename Derived>
	explicit DescriptorMap(GraphBase<Derived> const& graph, MapValueType const& value = MapValueType{})
	    : DescriptorMap(graph.template descriptors<Tag>(), value, descriptor_range)
	{}

	MapValueType& operator[](const_descriptor_t descriptor)
	{
		if (descriptor == nullptr) {
			throw std::invalid_argument("Cannot access descriptor map value at "
			                            "null descriptor.");
		}
		auto it = map_.find(descriptor);
		if (it == map_.end()) {
			std::tie(it, std::ignore) = map_.emplace(descriptor, defaultValue_);
		}
		return it->second;
	}

	MapValueType const& operator[](const_descriptor_t descriptor) const
	{
		if (descriptor == nullptr) {
			throw std::invalid_argument("Cannot access descriptor map value at "
			                            "null descriptor.");
		}
		auto it = map_.find(descriptor);
		if (it == map_.end()) {
			return defaultValue_;
		}
		return it->second;
	}

	auto&& getRange() { return map_; }

	auto&& getRange() const { return map_; }
};

/*! Descriptor map associated with SmallListElementStoragePolicy.
 *
 * Descriptor maps are always a small, sparse, unordered std::vector in this case, at least for now.
 */
template <typename Tag, typename ValueType, typename MapValueType>
class DescriptorMap<SmallListElementStoragePolicy, Tag, ValueType, MapValueType>
{
	static_assert(is_edge_or_vertex_tag_v<Tag>, "Tag must be either edge_tag or vertex_tag.");
	using storage_traits_t = ElementStorageTraits<ListElementStoragePolicy, Tag, ValueType>;
	using const_descriptor_t = typename storage_traits_t::const_descriptor_t;
	using pair_t = std::pair<const_descriptor_t, MapValueType>;
	using descriptor_map_t = std::vector<pair_t>;
	descriptor_map_t map_;

	const MapValueType defaultValue_{};

public:
	DescriptorMap() = default;

	template <typename T>
	DescriptorMap(T const& /*descriptors*/, DescriptorRangeTag const& /* dummy */)
	{}

	template <typename T>
	DescriptorMap(T const& /*descriptors*/, MapValueType const& value, DescriptorRangeTag const& /* dummy */)
	    : defaultValue_(value)
	{}

	template <typename Derived>
	explicit DescriptorMap(GraphBase<Derived> const& /*graph*/, MapValueType const& value = MapValueType{})
	    : defaultValue_(value)
	{}

	MapValueType& operator[](const_descriptor_t descriptor)
	{
		if (descriptor == nullptr) {
			throw std::invalid_argument("Cannot access descriptor map value at null descriptor.");
		}
		const auto e = map_.end();
		auto it = std::find_if(map_.begin(), e, [descriptor](auto& p) { return p.first == descriptor; });
		if (it != e) {
			return (*it).second;
		}
		// Not found, must append.
		map_.emplace_back(descriptor, defaultValue_);
		return map_.back().second;
	}

	MapValueType operator[](const_descriptor_t descriptor) const
	{
		if (descriptor == nullptr) {
			throw std::invalid_argument("Cannot access descriptor map value at null descriptor.");
		}
		const auto e = map_.end();
		auto it = std::find_if(map_.begin(), e, [descriptor](auto& p) { return p.first == descriptor; });
		if (it != e) {
			return (*it).second;
		}
		return defaultValue_;
	}

	auto&& getRange() { return map_; }

	auto&& getRange() const { return map_; }
};

} // namespace cxxgraph::policy
