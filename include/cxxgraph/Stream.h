// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"
#include "ListStorage.h"
#include "VectorStorage.h"

// Library Includes
// - none

// Standard Includes
#include <cstdint>
#include <iomanip>
#include <iosfwd>
#include <sstream>

namespace cxxgraph {
namespace policy {
	inline std::ostream& operator<<(std::ostream& os, VectorIndexPrintable const& d)
	{
		if (d.index != VectorIndexPrintable::invalid_contained_value) {
			os << d.index;
		} else {
			os << "InvalidIndex";
		}
		return os;
	}
	inline std::ostream& operator<<(std::ostream& os, PointerDescriptorPrintable const& d)
	{
		if (d.ptr == nullptr) {
			os << "nullptr";
		} else {
			std::ostringstream oss;
			oss << std::hex << std::setw(sizeof(void*) * 2) << std::setfill('0');
			oss << reinterpret_cast<std::uintptr_t>(d.ptr);
			os << oss.str();
		}
		return os;
	}
} // namespace policy

template <typename Tag, typename Derived>
inline std::ostream& operator<<(std::ostream& os, detail::DescriptorBase<Derived, Tag> const& d)
{
	static_assert(is_edge_or_vertex_tag_v<Tag>);
	if constexpr (std::is_same<Tag, edge_tag>()) {
		os << "E[";
	} else {
		os << "V[";
	}
	os << d.derived().printable();
	os << "]";
	return os;
}
} // namespace cxxgraph
