// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Base.h"
#include "Traversal.h"

// Library Includes
// - none

// Standard Includes
#include <functional>
#include <limits>
#include <queue>

#if __has_include(<optional>)
#include <optional>
namespace cxxgraph::traversal {
using std::optional;
} // namespace cxxgraph::traversal
#define GRAPH_TRAV_HAVE_OPTIONAL

#elif __has_include(<experimental/optional>)
#include <experimental/optional>
namespace cxxgraph::traversal {
using std::experimental::optional;
} // namespace cxxgraph::traversal
#define GRAPH_TRAV_HAVE_OPTIONAL
#endif

namespace cxxgraph::traversal {
using uint_cost_t = std::size_t;
constexpr uint_cost_t infinite_uint_cost = std::numeric_limits<uint_cost_t>::max();

struct DefaultCostPolicy
{
	using type = uint_cost_t;
	static constexpr type infinite = infinite_uint_cost;
	static constexpr type zero = 0;
	static constexpr type combine(type prevCost, type edgeCost) { return prevCost + edgeCost; }
	static constexpr bool better(type lhs, type rhs) { return lhs < rhs; }
};

namespace detail {
	template <typename VertexDescriptor, typename CostPolicy>
	struct QueueComparison
	{
		using cost_t = typename CostPolicy::type;
		using value_t = std::pair<cost_t, VertexDescriptor>;
		constexpr bool operator()(value_t const& lhs, value_t const& rhs) const
		{
			// a max-queue uses std::less for comparison, so if we want a min-queue we flip the order of the
			// arguments.
			return CostPolicy::better(rhs.first, lhs.first);
		}
	};
} // namespace detail
/*! Traversal strategy to perform a Dijkstra-shortest-path search/traversal.
 */
template <typename Graph, typename CostPolicy = DefaultCostPolicy>
struct DijkstraShortestPathTraversal : TraversalStrategyBase<DijkstraShortestPathTraversal<Graph, CostPolicy>, Graph>
{
	using graph_t = Graph;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<graph_t>;

	using cost_t = typename CostPolicy::type;

	using functor_t = std::function<cost_t(edge_const_descriptor_t)>;
	using predecessor_map_t = graph_descriptor_map_t<graph_t, vertex_tag, vertex_const_descriptor_t>;
	using preceding_edge_map_t = graph_descriptor_map_t<graph_t, vertex_tag, edge_const_descriptor_t>;
	using cost_map_t = graph_descriptor_map_t<graph_t, vertex_tag, cost_t>;
	using visited_map_t = graph_descriptor_map_t<graph_t, vertex_tag, bool>;

	//! Constructor
	explicit DijkstraShortestPathTraversal(Graph const& graph, functor_t&& getEdgeCostFunctor)
	    : getEdgeCost(std::move(getEdgeCostFunctor)), predecessors(graph), precedingEdges(graph),
	      costs(graph, CostPolicy::infinite), visited(graph, false)
	{}

	template <typename F>
	auto dequeueAndVisit(F&& earlyStoppingPredicate) -> state_vertex_descriptor_pair_t<graph_t>
	{
		if (queue.empty()) {
			return traversal_finished_v<graph_t>;
		}

		vertex_const_descriptor_t v;
		// Get the queued vertex with the minimum cost.
		// Ignore the cost, only stored for sorting purposes.
		std::tie(std::ignore, v) = queue.top();
		queue.pop();

		if (visited[v]) {
			return {ExecutionState::Skip, v};
		}

		visited[v] = true;

		// If we get here, v was an "open" vertex  - haven't necessarily enqueued all its children.
		if (earlyStoppingPredicate(v)) {
			// Let the early-stopping predicate quit once we know the best cost for v.
			return {ExecutionState::EarlyStop, v};
		}
		return {ExecutionState::Proceed, v};
	}

	/*! Overload for enqueue() that takes a predecessor vertex, a vertex to enqueue, and the cost that should be
	 * assigned.
	 *
	 * Used both in implementing the main enqueue() signature, as well as being useful for enqueuing roots with
	 * non-zero costs.
	 */
	auto enqueue(vertex_const_descriptor_t prev, vertex_const_descriptor_t v,
	             edge_const_descriptor_t e = edge_const_descriptor_t{}, cost_t cost = CostPolicy::zero)
	    -> ExecutionState
	{
		if (visited[v]) {
			// Only queue if we haven't already visited this vertex.
			// This does mean we might queue a vertex multiple times but that's OK,
			// since the first time we see it isn't guaranteed to be the minimum cost.
			return ExecutionState::Proceed;
		}
		if (CostPolicy::better(cost, costs[v])) {
			// If it's shorter/cheaper to go on this edge than our earlier-known path,
			// update cost and predecessor, and add to priority queue
			costs[v] = cost;
			predecessors[v] = prev;
			precedingEdges[v] = e;
			queue.emplace(cost, v);
		}
		return ExecutionState::Proceed;
	}

	template <typename F>
	auto enqueue(edge_const_descriptor_t e, vertex_const_descriptor_t prev, vertex_const_descriptor_t v, F &&
	             /*earlyStoppingPredicate*/) -> ExecutionState
	{
		if (visited[v]) {
			// Only queue if we haven't already visited this vertex.
			// This does mean we might queue a vertex multiple times but that's OK,
			// since the first time we see it isn't guaranteed to be the minimum cost.
			return ExecutionState::Proceed;
		}
		if (e == edge_const_descriptor_t{}) {
			// If edge is invalid, then this is a "root" which we'll enqueue as 0 cost.
			return enqueue(prev, v);
		}

		// This is a real edge (not being enqueued as "root" with 0 cost)
		cost_t costThruPrev = CostPolicy::combine(costs[prev], getEdgeCost(e));

		return enqueue(prev, v, e, costThruPrev);
	}

	/*! Overload for enqueueRoot() that takes a vertex to enqueue and the cost that should be  assigned.
	 *
	 * Useful for enqueuing roots with non-zero costs.
	 */
	auto enqueueRoot(vertex_const_descriptor_t v, cost_t cost = CostPolicy::zero) -> ExecutionState
	{
		return enqueue(v, v, edge_const_descriptor_t{}, cost);
	}

#ifdef GRAPH_TRAV_HAVE_OPTIONAL
	/*! Gets the minimum cost to reach vertex v, or an empty value if we cannot assert that we know the minimum
	 * cost.
	 */
	auto getMinCost(vertex_const_descriptor_t v) const -> optional<cost_t>
	{
		if (visited[v]) {
			return costs[v];
		}
		return {};
	}
	/*! Gets the edge in the min cost traversal used to reach vertex v, or an empty value if we cannot assert that
	 * we know the minimum cost.
	 */
	auto getPrecedingEdge(vertex_const_descriptor_t v) const -> optional<edge_const_descriptor_t>
	{
		if (visited[v]) {
			return precedingEdges[v];
		}
		return {};
	}
#endif
	functor_t getEdgeCost;
	predecessor_map_t predecessors;
	preceding_edge_map_t precedingEdges;
	cost_map_t costs;
	visited_map_t visited;
	using queue_element_t = std::pair<cost_t, vertex_const_descriptor_t>;
	using queue_comparison_t = detail::QueueComparison<vertex_const_descriptor_t, CostPolicy>;
	using queue_t = std::priority_queue<queue_element_t, std::vector<queue_element_t>, queue_comparison_t>;
	queue_t queue;
};
} // namespace cxxgraph::traversal
