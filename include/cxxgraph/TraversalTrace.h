// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Stream.h"
#include "Traversal.h"

// Library Includes
// - none

// Standard Includes
#include <sstream>

namespace cxxgraph::traversal {

/*! A "traversal strategy" that wraps a real traversal strategy, delegating operations but recording a string trace of
 * the procedure.
 *
 * Use by `auto myTraceStrategy = trace(realStrategy);` and then passing `myTraceStrategy` instead of `realStrategy`
 * to traverse()
 */
template <typename NestedStrategy>
struct TraversalTraceStrategy
    : TraversalStrategyBase<TraversalTraceStrategy<NestedStrategy>, graph_from_strategy_t<NestedStrategy>>
{
	using graph_t = graph_from_strategy_t<NestedStrategy>;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;
	using edge_const_descriptor_t = graph_edge_const_descriptor_t<graph_t>;
	std::reference_wrapper<NestedStrategy> nested;
	std::ostringstream os;

	constexpr explicit TraversalTraceStrategy(NestedStrategy& nested_) noexcept : nested(std::ref(nested_)) {}

	void beginVerticesConnectedTo(vertex_const_descriptor_t v)
	{
		os << v << " (\n";
		nested.get().beginVerticesConnectedTo(v);
	}

	void endVerticesConnectedTo(vertex_const_descriptor_t v)
	{
		os << ")\n";
		nested.get().endVerticesConnectedTo(v);
	}

	template <typename F>
	auto dequeueAndVisit(F&& earlyStoppingPredicate) -> state_vertex_descriptor_pair_t<graph_t>
	{
		vertex_const_descriptor_t v;
		cxxgraph::ExecutionState state;
		std::tie(state, v) = nested.get().dequeueAndVisit(std::forward<F>(earlyStoppingPredicate));

		os << "visit " << v << " (result: " << to_string(state) << ")\n";
		return {state, v};
	}

	template <typename F>
	auto enqueue(edge_const_descriptor_t e, vertex_const_descriptor_t prev, vertex_const_descriptor_t v,
	             F&& earlyStoppingPredicate) -> ExecutionState
	{
		auto state = nested.get().enqueue(e, prev, v, std::forward<F>(earlyStoppingPredicate));
		os << "  queued " << prev << " --" << e << "--> " << v;
		if (state != ExecutionState::Proceed) {
			os << " (result: " << to_string(state) << ")";
		}
		os << "\n";
		return state;
	}
};

//! Helper function to wrap a traversal strategy in a tracer, deducing all types.
template <typename NestedStrategy, typename Graph>
constexpr auto trace(TraversalStrategyBase<NestedStrategy, Graph>& strategy) -> TraversalTraceStrategy<NestedStrategy>
{
	return TraversalTraceStrategy<NestedStrategy>{strategy.derived()};
}
} // namespace cxxgraph::traversal
