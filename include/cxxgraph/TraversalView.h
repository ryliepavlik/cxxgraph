// Copyright 2018, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
/*!
 * @file
 * @brief  Header
 * @author Ryan Pavlik <ryan.pavlik@collabora.com>
 */

#pragma once

// Internal Includes
#include "Traversal.h"

// Library Includes
#include <range/v3/core.hpp>

// Standard Includes
// - none

#if __has_include(<optional>)
#include <optional>
namespace cxxgraph::traversal {
using std::optional;
} // namespace cxxgraph::traversal

#elif __has_include(<experimental/optional>)
#include <experimental/optional>

namespace cxxgraph::traversal {
using std::experimental::optional;
} // namespace cxxgraph::traversal
#else
#error "TraversalView requires optional or experimental/optional"
#endif

namespace cxxgraph::traversal {
template <typename TraversalStrategy>
class TraversalView : public ranges::view_facade<TraversalView<TraversalStrategy>>
{
	using graph_t = graph_from_strategy_t<TraversalStrategy>;
	using vertex_const_descriptor_t = graph_vertex_const_descriptor_t<graph_t>;
	friend ranges::range_access;
	optional<TraversalStrategy> strategy;
	optional<Traversal<TraversalStrategy>> trav;
	state_vertex_descriptor_pair_t<graph_t> state = traversal_finished_v<graph_t>;
	DirectedBehavior directedness = DirectedBehavior::Directed;

	struct cursor
	{
	private:
		TraversalView<TraversalStrategy>* rng_ = nullptr;

	public:
		using single_pass = std::true_type;

		cursor() = default;

		explicit cursor(TraversalView<TraversalStrategy>& rng) : rng_(&rng) {}

		void next() { rng_->next(); }

		vertex_const_descriptor_t const& read() const noexcept
		{
			return std::get<vertex_const_descriptor_t>(rng_->state);
		}
		// helper function
		ExecutionState getExecutionState() const noexcept
		{
			if (rng_ == nullptr) {
				return ExecutionState::Finished;
			}
			return std::get<ExecutionState>(rng_->state);
		}
		bool equal(ranges::default_sentinel) const noexcept { return shouldStop(getExecutionState()); }

		//! @todo this method only required for the old msvc-2015 fork of range-v3
		bool done() const noexcept { return shouldStop(getExecutionState()); }

		//! @todo this method only required for the old msvc-2015 fork of range-v3
		vertex_const_descriptor_t get() const noexcept { return read(); }
	};

	void next()
	{
		if (trav) {
			if constexpr (graph_is_bidirectional_directed_v<graph_t>) {
				state = trav->traversalStep(directedness, return_after_visiting_one);
			} else {
				state = trav->traversalStep(return_after_visiting_one);
			}
		}
	}

public:
	cursor begin_cursor() noexcept { return cursor{*this}; }
	TraversalView() = default;
	TraversalView(graph_t const& g, TraversalStrategy&& strategy_, DirectedBehavior directedness_)
	    : strategy(std::move(strategy_)), trav(Traversal<TraversalStrategy>{g, *strategy}),
	      directedness(directedness_)
	{
		static_assert(graph_is_bidirectional_directed_v<graph_t>,
		              "Can only supply a directedness if graph is bidirectional-directed");
		// "prime" state
		this->next();
	}

	TraversalView(graph_t const& g, TraversalStrategy&& strategy_)
	    : strategy(std::move(strategy_)), trav(Traversal<TraversalStrategy>{g, *strategy})
	{
		static_assert(!graph_is_bidirectional_directed_v<graph_t>,
		              "Must supply a directedness if graph is bidirectional-directed");
		// "prime" state
		this->next();
	}
};

/*! Creates a range view of vertex descriptors in the order visited by the strategy you provide.
 *
 * This overload moves from a provided strategy, which should already be initialized and have one or more vertices
 * enqueued using enqueueRoot(). No explicit type parameters (template arguments) are required.
 *
 * This overload only works on graphs that are not bidirectional-directed.
 */
template <typename TraversalStrategy, typename Graph>
constexpr auto traversalView(GraphBase<Graph> const& g, TraversalStrategy&& strategy)
    -> TraversalView<TraversalStrategy>
{
	static_assert(!graph_is_bidirectional_directed_v<Graph>,
	              "Must supply a directedness if graph is bidirectional-directed");
	static_assert(std::is_same<Graph, graph_from_strategy_t<TraversalStrategy>>::value,
	              "Graph must match the Strategy provided.");
	return {g.derived(), std::move(strategy)};
}

/*! Creates a range view of vertex descriptors in the order visited by the strategy you provide.
 *
 * This overload moves from a provided strategy, which should already be initialized and have one or more vertices
 * enqueued using enqueueRoot(). No explicit type parameters (template arguments) are required.
 *
 * This overload only works on graphs that are bidirectional-directed.
 */
template <typename TraversalStrategy, typename Graph>
constexpr auto traversalView(GraphBase<Graph> const& g, TraversalStrategy&& strategy, DirectedBehavior directedness)
    -> TraversalView<TraversalStrategy>
{
	static_assert(graph_is_bidirectional_directed_v<Graph>,
	              "Can only supply a directedness if graph is bidirectional-directed");
	static_assert(std::is_same<Graph, graph_from_strategy_t<TraversalStrategy>>::value,
	              "Graph must match the Strategy provided.");
	return {g.derived(), std::move(strategy), directedness};
}

/*! Creates a range view of vertex descriptors in the order visited by the strategy you provide, starting at the given
 * source vertex.
 *
 * This overload moves from a provided strategy instance after enqueuing the provided source vertex using enqueueRoot().
 * No explicit type parameters (template arguments) are required.
 *
 * This overload only works on graphs that are not bidirectional-directed.
 */
template <typename TraversalStrategy, typename Graph>
constexpr auto traversalView(GraphBase<Graph> const& g, TraversalStrategy&& strategy,
                             graph_vertex_const_descriptor_t<Graph> source) -> TraversalView<TraversalStrategy>
{
	static_assert(!graph_is_bidirectional_directed_v<Graph>,
	              "Must supply a directedness if graph is bidirectional-directed");
	static_assert(std::is_same<Graph, graph_from_strategy_t<TraversalStrategy>>::value,
	              "Graph must match the Strategy provided.");
	strategy.enqueueRoot(source);
	return {g.derived(), std::move(strategy)};
}

/*! Creates a range view of vertex descriptors in the order visited by the strategy you provide, starting at the given
 * source vertex.
 *
 * This overload moves from a provided strategy instance after enqueuing the provided source vertex using enqueueRoot().
 * No explicit type parameters (template arguments) are required.
 *
 * This overload only works on graphs that are bidirectional-directed.
 */
template <typename TraversalStrategy, typename Graph>
constexpr auto traversalView(GraphBase<Graph> const& g, TraversalStrategy&& strategy,
                             graph_vertex_const_descriptor_t<Graph> source, DirectedBehavior directedness)
    -> TraversalView<TraversalStrategy>
{
	static_assert(graph_is_bidirectional_directed_v<Graph>,
	              "Can only supply a directedness if graph is bidirectional-directed");
	static_assert(std::is_same<Graph, graph_from_strategy_t<TraversalStrategy>>::value,
	              "Graph must match the Strategy provided.");
	strategy.enqueueRoot(source);
	return {g.derived(), std::move(strategy), directedness};
}

/*! Creates a range view of vertex descriptors in the order visited by the strategy you provide, starting at the given
 * source vertex.
 *
 * This overload constructs the type of TraversalStrategy that you specify using the template parameter - the strategy
 * must be constructible from a graph. The provided source vertex is enqueued using enqueueRoot().
 *
 * This overload only works on graphs that are not bidirectional-directed.
 */
template <template <typename> class TraversalStrategyTemplate, typename Graph>
constexpr auto traversalView(GraphBase<Graph> const& g, graph_vertex_const_descriptor_t<Graph> source)
    -> TraversalView<TraversalStrategyTemplate<Graph>>
{
	static_assert(!graph_is_bidirectional_directed_v<Graph>,
	              "Must supply a directedness if graph is bidirectional-directed");
	static_assert(std::is_constructible<TraversalStrategyTemplate<Graph>, Graph const&>::value,
	              "Can only use the traversalView() overloads that don't take a Strategy instance with traversal "
	              "strategies that can be constructed from just a graph.");
	return traversalView(g.derived(), TraversalStrategyTemplate<Graph>{g.derived()}, source);
}

/*! Creates a range view of vertex descriptors in the order visited by the strategy you provide, starting at the given
 * source vertex.
 *
 * This overload constructs the type of TraversalStrategy that you specify using the template parameter - the strategy
 * must be constructible from a graph. The provided source vertex is enqueued using enqueueRoot().
 *
 * This overload only works on graphs that are bidirectional-directed.
 */
template <template <typename> class TraversalStrategyTemplate, typename Graph>
constexpr auto traversalView(GraphBase<Graph> const& g, graph_vertex_const_descriptor_t<Graph> source,
                             DirectedBehavior directedness) -> TraversalView<TraversalStrategyTemplate<Graph>>
{
	static_assert(graph_is_bidirectional_directed_v<Graph>,
	              "Can only supply a directedness if graph is bidirectional-directed");
	static_assert(std::is_constructible<TraversalStrategyTemplate<Graph>, Graph const&>::value,
	              "Can only use the traversalView() overloads that don't take a Strategy with traversal "
	              "strategies that can be constructed from just a graph.");
	return traversalView(g.derived(), TraversalStrategyTemplate<Graph>{g.derived()}, source, directedness);
}

} // namespace cxxgraph::traversal
